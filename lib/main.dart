import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:to_do_app/auth/sign_up.dart';
import 'package:to_do_app/bloc/account_bloc.dart';
import 'package:to_do_app/home_page.dart';
import 'package:to_do_app/auth/sign_in.dart';
import 'package:to_do_app/screens/settings_screen.dart';

import 'bloc/bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AppAccountBloc>(create: (context)=>AppAccountBloc()..createDatabaseForAuth()),
        BlocProvider<AppBloc>(create: (context) => AppBloc()..createDatabase(),)
      ],
      child: MaterialApp(
        theme: ThemeData(
         // primaryColor: Colors.green,
          focusColor: Colors.green,
          scaffoldBackgroundColor: Colors.white,
          appBarTheme:AppBarTheme(
            backgroundColor: Colors.green,
            systemOverlayStyle: SystemUiOverlayStyle(
              statusBarColor: Colors.green[600]
            )
          ),
          bottomNavigationBarTheme: BottomNavigationBarThemeData(
            backgroundColor: Colors.green,
            //selectedItemColor: Colors.lightGreen,
            selectedIconTheme: IconThemeData(
              color: Colors.black
            ),


          ),
        ),
        darkTheme: ThemeData(
          appBarTheme: AppBarTheme(
            color: Color(0xFF58555d),
            elevation: 0.5

          ),
          scaffoldBackgroundColor: Color(0xFF454349),
          bottomNavigationBarTheme: BottomNavigationBarThemeData(
            backgroundColor: Color(0xFF58555d),
          )
        ),
        themeMode: ThemeMode.light,
        debugShowCheckedModeBanner: false,
        home: SignInScreen(),
        routes: {
          "HomePage":(context)=>HomePage(),
          "SignInScreen":(context)=>SignInScreen(),
          "SignUpScreen":(context)=>SignUpScreen(),
          "Settings":(context)=>Settings()
        },
      ),
    );
  }
}


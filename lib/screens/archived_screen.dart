import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:to_do_app/bloc/bloc.dart';
import 'package:to_do_app/bloc/to_do_state.dart';
import 'package:to_do_app/components/components.dart';

class ArchivedScreen extends StatefulWidget {
  const ArchivedScreen({Key? key}) : super(key: key);

  @override
  _ArchivedScreenState createState() => _ArchivedScreenState();
}

class _ArchivedScreenState extends State<ArchivedScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppBloc, AppStates>(
        listener: (context, state) {
          if(state is AppDeleteFromDataAtDatabaseState)
          {
            Navigator.pop(context);
          }
        },
        builder: (context, state) {
          var blocContext=context;
          AppBloc bloc = AppBloc.get(context);
          int unicode = 0x1F60A;
          if (bloc.archivedDatabase.length > 0 || bloc.archivedDatabase.isNotEmpty) {
            var task = bloc.archivedDatabase;
            return ListView.separated(
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.all(8),
                    child: Row(
                      children: [
                        iconButton(task,index,context,blocContext),
                        Expanded(
                          child: CheckboxListTile(
                              title: Text("${task[index]['goal']}"),
                              subtitle: Text(
                                  "in ${task[index]['date']} at ${task[index]['time']}"),
                              controlAffinity: ListTileControlAffinity.leading,
                              activeColor: Colors.green,
                              secondary: CircleAvatar(
                                child: Text("${task[index]['id']}"),
                                backgroundColor: Colors.green,
                              ),
                              value: bloc.getArchivedIsChecked(index),
                              onChanged: (value) {
                                bloc.updateDatabase(
                                    status: 'new', id: task[index]['id']);
                                bloc.changeArchivedCheckbox(value, index);
                              }),
                        ),
                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index) => Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Container(
                    color: Colors.green,
                    height: 0.5,
                  ),
                ),
                itemCount: task.length);
          } else {
            return Center(
              child: Text("\uD83D\uDE00 \uD83D\uDC4C",textAlign: TextAlign.center,style: TextStyle(fontSize: 50),),
            );
          }
        });
  }
}

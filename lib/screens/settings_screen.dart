import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:to_do_app/bloc/account_bloc.dart';
import 'package:to_do_app/bloc/account_bloc_states.dart';
import 'package:to_do_app/components/components.dart';

class Settings extends StatelessWidget {
  Settings({Key? key}) : super(key: key);
  var emailController = TextEditingController();
  var userNameController = TextEditingController();
  var nameController = TextEditingController();
  var lastNameController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  var scaffoldKey = GlobalKey<ScaffoldState>();

  int? value;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppAccountBloc, AppAccountState>(
        listener: (context, state) {},
        builder: (context, state) {
          AppAccountBloc bloc = AppAccountBloc.get(context);
          return Scaffold(
            key: scaffoldKey,
            body: CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  pinned: true,
                  snap: true,
                  floating: true,
                  expandedHeight: 160.0,
                  flexibleSpace: FlexibleSpaceBar(
                    title: ListTile(
                      title: Text(bloc.getAuthData(bloc.index, 'userName'),style: TextStyle(color: Colors.white),),
                      leading: CircleAvatar(
                        child: Text(
                          bloc.getAuthData(bloc.index, "name")[0].toUpperCase(),
                          style: TextStyle(fontSize: 25),
                        ),
                      ),
                      subtitle: Text(bloc.getAuthData(bloc.index, "name")),
                    ),
                    titlePadding: EdgeInsets.only(left: 30, top: 15),
                  ),
                  //title: Text("Settings"),
                  leading: IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
                SliverList(
                  delegate: SliverChildBuilderDelegate((context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 200,
                            width: double.infinity,
                            child: Card(
                              shape:RoundedRectangleBorder(
                                borderRadius: borderRadius(),
                              ) ,
                              color: Colors.pink,
                              child: Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Column(
                                    children: [
                                      Text(
                                        "Account",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        "Email:" +
                                            bloc.getAuthData(
                                                bloc.index, "email"),
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        "UserName:" +
                                            bloc.getAuthData(
                                                bloc.index, 'userName'),
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      buildButton(
                                          onPressed: () {
                                            showModalBottomSheet(
                                              context: context,
                                              isScrollControlled: true,
                                              isDismissible: false,
                                              barrierColor:
                                                  Colors.green.withOpacity(0.2),
                                              builder: (context) => Padding(
                                                padding: MediaQuery.of(context)
                                                    .viewInsets,
                                                child: Container(
                                                  padding: EdgeInsets.only(
                                                      top: 2,
                                                      left: 10,
                                                      right: 10,
                                                      bottom: 10),
                                                  child: Form(
                                                    key: formKey,
                                                    child: Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Center(
                                                          child: Icon(
                                                            Icons
                                                                .arrow_drop_down_outlined,
                                                            size: 30,
                                                            color: Colors.green,
                                                          ),
                                                        ),
                                                        buildFormField(
                                                          title: "Email",
                                                          controller:
                                                              emailController,
                                                          prefixIcon:
                                                              Icons.email,
                                                          validator: (value) {
                                                            if (value!
                                                                    .isEmpty ||
                                                                value.length ==
                                                                    0) {
                                                              return "Enter email";
                                                            } else if (bloc
                                                                .findEmailForSettings(
                                                                    emailController
                                                                        .text)) {
                                                              return "Your email is already used";
                                                            } else
                                                              return null;
                                                          },
                                                          focusNode: bloc
                                                              .getFocusNode(1),
                                                          prefixIconColor: bloc
                                                              .getPrefixIconColor(
                                                                  1),
                                                        ),
                                                        SizedBox(
                                                          height: 20,
                                                        ),
                                                        buildFormField(
                                                          title: "UserName",
                                                          controller:
                                                              userNameController,
                                                          prefixIcon:
                                                              Icons.person,
                                                          validator: (value) {
                                                            if (value!
                                                                    .isEmpty ||
                                                                value.length ==
                                                                    0) {
                                                              return "Enter username";
                                                            } else if (bloc
                                                                .findUserName(
                                                                    userNameController
                                                                        .text)) {
                                                              return "Your username is already used";
                                                            } else
                                                              return null;
                                                          },
                                                          focusNode: bloc
                                                              .getFocusNode(2),
                                                          prefixIconColor: bloc
                                                              .getPrefixIconColor(
                                                                  2),
                                                        ),
                                                        SizedBox(
                                                          height: 30,
                                                        ),
                                                        Center(
                                                            child: buildButton(
                                                                onPressed: () {
                                                                  if (formKey
                                                                      .currentState!
                                                                      .validate()) {
                                                                    bloc.update(
                                                                        email: emailController
                                                                            .text,
                                                                        userName:
                                                                            userNameController
                                                                                .text,
                                                                        id: bloc
                                                                            .getAccountId());
                                                                    Navigator.pop(
                                                                        context);
                                                                    emailController =
                                                                        TextEditingController();
                                                                    userNameController =
                                                                        TextEditingController();
                                                                    print(bloc
                                                                        .authData());
                                                                      ScaffoldMessenger.of(context).showSnackBar(snackBar("Email and username updated"));
                                                                  }
                                                                },
                                                                buttonTitle:
                                                                    "Update"))
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              backgroundColor: Colors.white,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                  borderRadius(),
                                                  side: BorderSide(
                                                      color: Colors.green,
                                                      width: 2)),
                                            );
                                          },
                                          buttonTitle: 'Update')
                                    ],
                                  )),
                            ),
                          ),
                          Container(
                            height: 200,
                            width: double.infinity,
                            child: Card(
                              color: Colors.pink,
                              shape: RoundedRectangleBorder(
                                borderRadius: borderRadius()
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: [
                                    Text(
                                      "personality info",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      "Name:" +
                                          bloc.getAuthData(bloc.index, "name"),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      "Last name: " +
                                          bloc.getAuthData(
                                              bloc.index, "lastName"),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    buildButton(
                                        onPressed: () {
                                          showModalBottomSheet(
                                            context: context,
                                            isDismissible: false,
                                            isScrollControlled: true,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: borderRadius(),
                                              side: BorderSide(
                                                width: 2,
                                                color: Colors.green
                                              )
                                            ),
                                            barrierColor:
                                                Colors.green.withOpacity(0.2),
                                            builder: (context) {
                                              return Padding(
                                                padding: MediaQuery.of(context)
                                                    .viewInsets,
                                                child: Container(
                                                  padding: EdgeInsets.only(
                                                      top: 2,
                                                      left: 10,
                                                      right: 10,
                                                      bottom: 10),
                                                  child: Form(
                                                    key: formKey,
                                                    child: Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Center(
                                                          child: Icon(
                                                            Icons
                                                                .arrow_drop_down_outlined,
                                                            size: 30,
                                                            color: Colors.green,
                                                          ),
                                                        ),
                                                        buildFormField(
                                                          focusNode: bloc
                                                              .getFocusNode(0),
                                                          prefixIconColor: bloc
                                                              .getPrefixIconColor(
                                                                  0),
                                                          validator: (value) {
                                                            if (value!
                                                                    .isEmpty ||
                                                                value.length ==
                                                                    0) {
                                                              return "Plz enter your name";
                                                            } else if (value
                                                                    .length >=
                                                                10) {
                                                              return "Your name must be less than 10 characters";
                                                            } else
                                                              return null;
                                                          },
                                                          prefixIcon:
                                                              Icons.person_add,
                                                          title: "Add name",
                                                          controller:
                                                              nameController,
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        buildFormField(
                                                          focusNode: bloc
                                                              .getFocusNode(1),
                                                          prefixIconColor: bloc
                                                              .getPrefixIconColor(
                                                                  1),
                                                          validator: (value) {
                                                            if (value!
                                                                    .isEmpty ||
                                                                value.length ==
                                                                    0) {
                                                              return "Plz enter your last name";
                                                            } else if (value
                                                                    .length >=
                                                                10) {
                                                              return "Your last name must be less than 10 characters";
                                                            } else
                                                              return null;
                                                          },
                                                          prefixIcon:
                                                              Icons.person_add,
                                                          title:
                                                              "Add last name",
                                                          controller:
                                                              lastNameController,
                                                        ),
                                                        Center(
                                                          child: buildButton(
                                                              onPressed: () {
                                                                if (formKey
                                                                    .currentState!
                                                                    .validate()) {
                                                                  bloc.updatePersonalityInf(
                                                                      name: nameController
                                                                          .text,
                                                                      lastName:
                                                                          lastNameController
                                                                              .text,
                                                                      id: bloc
                                                                          .getAccountId());
                                                                  Navigator.pop(
                                                                      context);
                                                                  ScaffoldMessenger.of(context).showSnackBar(snackBar('Name and lastname updated '));
                                                                }
                                                              },
                                                              buttonTitle:
                                                                  'Add'),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              );
                                            },
                                          );
                                        },
                                        buttonTitle: "Add")
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                            height: 200,
                            width: double.infinity,
                            child: Card(
                              color: Colors.pink,
                               shape: RoundedRectangleBorder(
                                borderRadius: borderRadius()
                               ),
                              child: Center(
                                child: Text(
                                  "Made With ❤️",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  }, childCount: 1),
                ),
              ],
            ),
          );
        });
  }
  SnackBar snackBar(String value)
  {
    return SnackBar(
      content: Text(value),
      duration: Duration(seconds: 3),
    );
  }
}

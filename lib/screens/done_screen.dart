import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:to_do_app/bloc/bloc.dart';
import 'package:to_do_app/bloc/to_do_state.dart';
import 'package:to_do_app/components/components.dart';

class DoneScreen extends StatefulWidget {
  const DoneScreen({Key? key}) : super(key: key);

  @override
  _DoneScreenState createState() => _DoneScreenState();
}

class _DoneScreenState extends State<DoneScreen> {
  @override
  Widget build(BuildContext context) {
    BuildContext contextScaffold=context;
    return BlocConsumer<AppBloc, AppStates>(listener: (context, state) {
      if (state is AppDeleteFromDataAtDatabaseState) {
        Navigator.pop(context);
      }
    }, builder: (context, state) {
      var blocContext = context;
      AppBloc bloc = AppBloc.get(context);
      int unicode = 0x1F60A;
      if (bloc.doneDatabase.length > 0 || bloc.doneDatabase.isNotEmpty) {
        var task = bloc.doneDatabase;
        return ListView.separated(
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return Dismissible(
                key: UniqueKey(),
                direction: DismissDirection.horizontal,
                onDismissed: (direction) {
                  if(direction == DismissDirection.startToEnd)
                    {
                      bloc.updateDatabase(
                          status: 'archived', id: task[index]['id']);
                    }
                  else if (direction == DismissDirection.endToStart)
                    {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              backgroundColor: Colors.white,
                              title: Text(
                                "Do you want to update this task?",
                                style: TextStyle(fontSize: 15),
                              ),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(20),
                                      bottomLeft: Radius.circular(20)),
                                  side: BorderSide(color: Colors.green, width: 2)),
                              contentPadding: EdgeInsets.only(top: 20),
                              content: Container(
                                height: 50,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                    color: Colors.green,
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(20)),
                                    border: Border.all(
                                        width: 2.5, color: Colors.white)),
                                child: InkWell(
                                  child: Center(
                                      child: Text(
                                        "Update",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 17,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      )),
                                  onTap: (){
                                    Navigator.pop(context);
                                    Scaffold.of(contextScaffold).showBottomSheet<void>((context){
                                      return Container(
                                        height: 300,
                                        color: Colors.white,
                                        child:buildUpdate(contextScaffold,blocContext,task[index]),
                                      );
                                    });
                                  },
                                ),
                              ),
                            );
                          });
                    }
                  // bloc.updateDatabase(
                  //     status: 'archived', id: task[index]['id']);
                },
                background: Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(left: 20.0),
                  color: Colors.blue,
                  child: Icon(Icons.archive_outlined, color: Colors.white),
                ),
                secondaryBackground:Container(
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.only(right: 20.0),
                  color: Colors.green,
                  child: Icon(Icons.update, color: Colors.white),
                ),
                child: Container(
                  margin: EdgeInsets.all(8),
                  child: Row(
                    children: [
                      // iconButton(task[index], context),
                      iconButton(task, index, context, blocContext),
                      Expanded(
                        child: CheckboxListTile(
                            title: Text("${task[index]['goal']}"),
                            subtitle: Text(
                                "in ${task[index]['date']} at ${task[index]['time']}"),
                            controlAffinity: ListTileControlAffinity.leading,
                            activeColor: Colors.green,
                            secondary: CircleAvatar(
                              child: Text("${task[index]['id']}"),
                              backgroundColor: Colors.green,
                            ),
                            value: bloc.getDoneIsChecked(index),
                            onChanged: (value) {
                              bloc.updateDatabase(
                                  status: 'new', id: task[index]['id']);
                              bloc.changeDoneCheckbox(value, index);
                            }),
                      ),
                    ],
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) => Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Container(
                    color: Colors.green,
                    height: 0.5,
                  ),
                ),
            itemCount: task.length);
      } else {
        return Center(
          child: Text(
            "\uD83D\uDE00 \uD83D\uDC4C",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 50),
          ),
        );
      }
    });
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:to_do_app/bloc/account_bloc.dart';
import 'package:to_do_app/bloc/account_bloc_states.dart';
import 'package:to_do_app/bloc/bloc.dart';
import 'package:to_do_app/bloc/to_do_state.dart';

class HomePage extends StatelessWidget {
  bool isTimeFormFieldEnabled = true;
  bool isDateFormFieldEnabled = true;
  var timeController = TextEditingController();
  var goalController = TextEditingController();
  var dateController = TextEditingController();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  final formFieldKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppBloc, AppStates>(
      builder: (context, state) {
        AppBloc bloc = AppBloc.get(context);
        return Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            title: Text(bloc.getTitle()),
          ),
          drawer: BlocConsumer<AppAccountBloc,AppAccountState>(
            listener: (context,state){},
            builder: (context,state){
              AppAccountBloc authBloc =AppAccountBloc.get(context);
              return Drawer(
                child: Column(
                  children: [
                    UserAccountsDrawerHeader(
                      currentAccountPicture: CircleAvatar(
                        child: Text(authBloc.getAuthData(authBloc.index, "name")[0].toUpperCase(),style: TextStyle(fontSize: 25),),
                      ),
                      accountName: Text(authBloc.getAuthData(authBloc.index, "name")),
                      accountEmail: Text(authBloc.getAuthData(authBloc.index, "email")),
                    ),
                    ListTile(
                      title: Text("settings"),
                      leading: Icon(Icons.settings),
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed("Settings");
                      },
                    ),
                    ListTile(
                      title: Text("Log Out"),
                      leading: Icon(Icons.logout),
                      onTap: () {
                        Navigator.of(context)
                            .pushReplacementNamed("SignInScreen");
                      },
                    ),
                  ],
                ),
              );
            },
          ),
          body: bloc.getScreens(),
          floatingActionButton: FloatingActionButton(
            child: Icon(bloc.getIcon()),
            onPressed: () {
              if (bloc.getIsOpen()) {
                if (formFieldKey.currentState!.validate()) {
                  bloc.insertToDatabase(
                      goal: goalController.text,
                      date: dateController.text,
                      time: timeController.text,
                  );
                  bloc.changeFloatIcon(false, Icons.edit);
                }
              } else {
                scaffoldKey.currentState!
                    .showBottomSheet((context) {
                      return Container(
                        padding: EdgeInsets.all(10),
                        color: Colors.white,
                        child: Form(
                          key: formFieldKey,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Row(
                                children: [
                                  IconButton(
                                    icon: Icon(Icons.assignment_outlined),
                                    color: Colors.green,
                                    onPressed: () {},
                                  ),
                                  Expanded(
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.green)),
                                          hintText: "Type your goal",
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                            color: Colors.green,
                                          ))),
                                      cursorColor: Colors.green,
                                      controller: goalController,
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return "You have to writ a goal";
                                        }
                                        return null;
                                      },
                                      maxLength: 20,
                                      autocorrect: true,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  IconButton(
                                    icon: Icon(Icons.access_time_outlined),
                                    color: Colors.green,
                                    onPressed: () {
                                      showTimePicker(
                                              context: context,
                                              initialTime: TimeOfDay.now())
                                          .then((value) {
                                        timeController.text =
                                            value!.format(context).toString();
                                      });
                                    },
                                  ),
                                  Expanded(
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                          enabled: isTimeFormFieldEnabled,
                                          disabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.green)),
                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.green)),
                                          hintText: "Inter your goal's time",
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                            color: Colors.green,
                                          ))),
                                      cursorColor: Colors.green,
                                      controller: timeController,
                                      keyboardType: TextInputType.datetime,
                                      validator: (value) {
                                        {
                                          if (value == null || value.isEmpty) {
                                            // setState(() {
                                            //   isTimeFormFieldEnabled = true;
                                            // });
                                            return "pleas enter your goal's time";
                                          } else {
                                            // setState(() {
                                            //   isTimeFormFieldEnabled = false;
                                            // });
                                          }
                                        }
                                        return null;
                                      },
                                      autocorrect: true,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  IconButton(
                                    icon: Icon(Icons.calendar_today_outlined),
                                    color: Colors.green,
                                    onPressed: () {
                                      showDatePicker(
                                              context: context,
                                              initialDate: DateTime.now(),
                                              firstDate: DateTime.now(),
                                              lastDate: DateTime(2022))
                                          .then((value) => dateController.text =
                                              DateFormat.yMd()
                                                  .format(value as DateTime)
                                                  .toString());
                                    },
                                  ),
                                  Expanded(
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                          enabled: isDateFormFieldEnabled,
                                          disabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.green)),
                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.green)),
                                          hintText: "Inter your goal's date",
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                            color: Colors.green,
                                          ))),
                                      cursorColor: Colors.green,
                                      controller: dateController,
                                      keyboardType: TextInputType.datetime,
                                      validator: (value) {
                                        {
                                          if (value == null || value.isEmpty) {
                                            // setState(() {
                                            //   isDateFormFieldEnabled = true;
                                            // });
                                            return "pleas enter your goal's date";
                                          } else {
                                            // setState(() {
                                            //   isDateFormFieldEnabled = false;
                                            // });
                                          }
                                        }
                                        return null;
                                      },
                                      autocorrect: true,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    })
                    .closed
                    .then((value) {
                      bloc.changeFloatIcon(false, Icons.edit);
                    });
                bloc.changeFloatIcon(true, Icons.add);
              }
            },
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: bloc.getCurrentIndex(),
            selectedLabelStyle: TextStyle(color: Colors.yellowAccent),
            onTap: (index) {
              bloc.changeIndex(index);
            },
            items: [
              BottomNavigationBarItem(
                  icon: Container(
                      width: 55,
                      height: 30,
                      decoration: BoxDecoration(
                          color: bloc.getBottomNvColor(0),
                          borderRadius: BorderRadius.circular(10)),
                      child: Icon(Icons.local_fire_department_outlined)),
                  label: "Goal"),
              BottomNavigationBarItem(
                  icon: Container(
                      width: 55,
                      height: 30,
                      decoration: BoxDecoration(
                          color: bloc.getBottomNvColor(1),
                          borderRadius: BorderRadius.circular(10)),
                      child: Icon(Icons.check_circle_outline)),
                  label: "Done"),
              BottomNavigationBarItem(
                  icon: Container(
                      width: 55,
                      height: 30,
                      decoration: BoxDecoration(
                          color: bloc.getBottomNvColor(2),
                          borderRadius: BorderRadius.circular(10)),
                      child: Icon(Icons.archive_outlined)),
                  label: "archived"),
            ],
          ),
        );
      },
      listener: (context, state) {
        if (state is AppInsertToDatabaseState) {
          Navigator.pop(context);
          timeController=TextEditingController();
          goalController=TextEditingController();
          dateController=TextEditingController();
        }
      },
    );
  }
}

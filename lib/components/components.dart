import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:to_do_app/bloc/bloc.dart';

// Widget showDialog({context, AlertDialog Function(context) builder})
// {
//   return showDialog(
//     context: context,
//     builder: (context) {
//       return AlertDialog(
//         title: Text("Delete this conversation?"),
//         content: Text("This is permanet and can't be undone"),
//         actions: [
//           TextButton(
//               onPressed: () {
//                 Navigator.pop(context);
//               },
//               child: Text("Cancel")),
//           TextButton(
//               onPressed: () {
//                 AppBloc.get(blocContext)
//                     .deleteFromDatabase(id: model[index]['id']);
//               },
//               child: Text("Delete")),
//         ],
//       );
//     },
//   );
// }

Widget iconButton(List<Map> model, index, context, blocContext) {
  return IconButton(
      onPressed: () {
        showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text("Delete this conversation?"),
              content: Text("This is permanet and can't be undone"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("Cancel")),
                TextButton(
                    onPressed: () {
                      AppBloc.get(blocContext)
                          .deleteFromDatabase(id: model[index]['id']);
                    },
                    child: Text("Delete")),
              ],
            );
          },
        );
      },
      icon: Icon(
        Icons.delete_outline,
        color: Colors.red,
      ));
}

Widget buildScreen(
    List<Map> task, int index, BuildContext context, BuildContext blocContext) {
  return Dismissible(
    key: UniqueKey(),
    onDismissed: (direction) {
      AppBloc().updateDatabase(status: 'archived', id: task[index]['id']);
    },
    child: Container(
      margin: EdgeInsets.all(8),
      child: Row(
        children: [
          iconButton(task, index, context, blocContext),
          Expanded(
            child: CheckboxListTile(
                title: Text("${task[index]['goal']}"),
                subtitle:
                    Text("in ${task[index]['date']} at ${task[index]['time']}"),
                controlAffinity: ListTileControlAffinity.leading,
                activeColor: Colors.green,
                secondary: CircleAvatar(
                  child: Text("${task[index]['id']}"),
                  backgroundColor: Colors.green,
                ),
                value: AppBloc().getGoalIsChecked(index),
                onChanged: (value) {
                  AppBloc()
                      .updateDatabase(status: 'done', id: task[index]['id']);
                  AppBloc().changeGoalCheckbox(value, index);
                }),
          ),
        ],
      ),
    ),
  );
}

Widget buildUpdate(
    BuildContext scaffoldContext, BuildContext blocContext, Map list) {
  var timeController = TextEditingController();
  var goalController = TextEditingController();
  var dateController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  return Form(
    key: formKey,
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            IconButton(
              icon: Icon(Icons.assignment_outlined),
              color: Colors.green,
              onPressed: () {},
            ),
            Expanded(
              child: TextFormField(
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green)),
                    hintText: "Type your goal",
                    enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                      color: Colors.green,
                    ))),
                cursorColor: Colors.green,
                controller: goalController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "You have to writ a goal";
                  }
                  return null;
                },
                maxLength: 20,
                autocorrect: true,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          children: [
            IconButton(
              icon: Icon(Icons.access_time_outlined),
              color: Colors.green,
              onPressed: () {
                showTimePicker(
                        context: scaffoldContext, initialTime: TimeOfDay.now())
                    .then((value) {
                  timeController.text =
                      value!.format(scaffoldContext).toString();
                });
              },
            ),
            Expanded(
              child: TextFormField(
                decoration: InputDecoration(
                    disabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green)),
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green)),
                    hintText: "Inter your goal's time",
                    enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                      color: Colors.green,
                    ))),
                cursorColor: Colors.green,
                controller: timeController,
                keyboardType: TextInputType.datetime,
                validator: (value) {
                  {
                    if (value == null || value.isEmpty) {
                      // setState(() {
                      //   isTimeFormFieldEnabled = true;
                      // });
                      return "pleas enter your goal's time";
                    } else {
                      // setState(() {
                      //   isTimeFormFieldEnabled = false;
                      // });
                    }
                  }
                  return null;
                },
                autocorrect: true,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 30,
        ),
        Row(
          children: [
            IconButton(
              icon: Icon(Icons.calendar_today_outlined),
              color: Colors.green,
              onPressed: () {
                showDatePicker(
                        context: scaffoldContext,
                        initialDate: DateTime.now(),
                        firstDate: DateTime.now(),
                        lastDate: DateTime(2022))
                    .then((value) => dateController.text =
                        DateFormat.yMd().format(value as DateTime).toString());
              },
            ),
            Expanded(
              child: TextFormField(
                decoration: InputDecoration(
                    disabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green)),
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green)),
                    hintText: "Inter your goal's date",
                    enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                      color: Colors.green,
                    ))),
                cursorColor: Colors.green,
                controller: dateController,
                keyboardType: TextInputType.datetime,
                validator: (value) {
                  {
                    if (value == null || value.isEmpty) {
                      return "pleas enter your goal's date";
                    }
                  }
                  return null;
                },
                autocorrect: true,
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        SizedBox(
            width: 200,
            height: 50,
            child: buildButton(
                onPressed: () {
                  if (formKey.currentState!.validate()) {
                    formKey.currentState!.save();
                    AppBloc.get(blocContext).update(
                        date: dateController.text,
                        goal: goalController.text,
                        time: timeController.text,
                        id: list['id']);
                    Navigator.pop(scaffoldContext);
                    ScaffoldMessenger.of(scaffoldContext).showSnackBar(SnackBar(
                      content: Text("Updated"),
                      duration:Duration(seconds: 3),
                    ));
                  }
                },
                buttonTitle: "Update"))
      ],
    ),
  );
}

Widget buildButton({
  @required void Function()? onPressed,
  @required String? buttonTitle,
}) {
  return ElevatedButton(
    onPressed: onPressed,
    child: Text("$buttonTitle"),
    style: ElevatedButton.styleFrom(
        primary: Colors.green,
        side: BorderSide(width: 2, color: Colors.white),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                bottomRight: Radius.circular(20))),
        textStyle: TextStyle(fontSize: 17)),
  );
}

Widget buildFormField({
  @required Color? prefixIconColor,
  @required IconData ?prefixIcon,
  @required FocusNode? focusNode,
  @required String? title,
  @required String? Function(String?)? validator,
  @required TextEditingController? controller,
  bool isPasswordShow = false,
  void Function(String)? onChanged,
  TextInputType? keyboardType,
  Widget? suffixIcon,
}) {
  return TextFormField(
    validator: validator,
    onChanged: onChanged,
    focusNode: focusNode,
    controller: controller,
    keyboardType: keyboardType,
    cursorColor: Colors.green,
    obscureText: isPasswordShow,
    decoration: InputDecoration(
        suffixIcon: suffixIcon,
        prefixIcon: Icon(
          prefixIcon,
          color:
              prefixIconColor, //_focusNode.hasFocus ? Colors.green : Colors.black,
        ),
        hintText: title,
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.lightGreen, width: 2),
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(20),
                topLeft: Radius.circular(20))),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green, width: 2),
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(20),
                topLeft: Radius.circular(20)))),
  );
}
BorderRadiusGeometry borderRadius(){
  return BorderRadius.only(topLeft: Radius.circular(20),bottomRight:Radius.circular(20) );
}
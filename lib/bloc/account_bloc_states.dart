abstract class AppAccountState {}

class AppInitialState extends AppAccountState {}

class AppFocusNodesState extends AppAccountState {}

class AppSuffixIconIsNull extends AppAccountState {}

class AppSuffixIconIsNotNull extends AppAccountState {}

class AppPasswordIconMoodTrue extends AppAccountState {}

class AppPasswordIconMoodFalse extends AppAccountState {}

class AppCreateDatabaseState extends AppAccountState {}

class AppInsertInToDatabaseState extends AppAccountState {}

class AppGetFromAuthDatabaseState extends AppAccountState {}

class AppUpdateAuthDatabaseState extends AppAccountState {}

class AppIsPasswordShowState extends AppAccountState {}

class AppUpdatePersonalityIfState extends AppAccountState {}

class AppSignUpState extends AppAccountState {}



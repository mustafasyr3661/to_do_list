abstract class AppStates {}

class AppInitialState extends AppStates {}

class AppGoalCheckBoxState extends AppStates {}

class AppDoneCheckBoxState extends AppStates {}

class AppArchivedCheckBoxState extends AppStates {}

class AppChangeBottomNavBarState extends AppStates {}

class AppFloatState extends AppStates {}

class AppCreateDatabaseState extends AppStates {}

class AppInsertToDatabaseState extends AppStates {}

class AppGetFromDatabase extends AppStates {}

class AppUpdateDataAtDatabaseState extends AppStates {}

class AppUpdateTasksDataAtDatabaseState extends AppStates {}

class AppDeleteFromDataAtDatabaseState extends AppStates {}

class AppIsCheckState extends AppStates {}

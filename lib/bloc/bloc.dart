import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sqflite/sqflite.dart';
import 'package:to_do_app/bloc/to_do_state.dart';
import 'package:to_do_app/screens/archived_screen.dart';
import 'package:to_do_app/screens/done_screen.dart';
import 'package:to_do_app/screens/goal_screen.dart';

  class AppBloc extends Cubit<AppStates> {
  AppBloc() : super(AppInitialState());

  static AppBloc get(context) => BlocProvider.of(context);
  Database? _db;
  IconData _floatIcon = Icons.edit;
  int _currentIndex = 0;
  bool _isBottomSheetOpen = false;
  List<String> _titles = ['Goals', 'Done', 'Archived'];
  List<Widget> _screens = [GoalScreen(), DoneScreen(), ArchivedScreen()];
  List<Map> goalDatabase = [];
  List<Map> doneDatabase = [];
  List<Map> archivedDatabase = [];
  List<bool> _isGoalChecked = [];
  List<bool> _isDoneChecked = [];
  List<bool> _isArchivedChecked = [];

  //====================================================
  static final columnGoal = 'goal';
  static final columnDate = 'date';
  static final columnTime = 'time';
  static final columnStatus = 'status';
  static final columnId = 'id';
  static final columnUserAc = 'user';
  static final table = 'tasks';

  //====================================================
  void changeIndex(int? index) {
    _currentIndex = index!;
    emit(AppChangeBottomNavBarState());
  }

  void changeGoalCheckbox(bool? value, int index) {
    _isGoalChecked[index] = value!;
    emit(AppGoalCheckBoxState());
  }

  void changeArchivedCheckbox(bool? value, int index) {
    _isArchivedChecked[index] = value!;
    emit(AppArchivedCheckBoxState());
  }

  void changeDoneCheckbox(bool? value, int index) {
    _isDoneChecked[index] = value!;
    emit(AppGoalCheckBoxState());
  }

  void changeFloatIcon(bool? value, IconData fapIcon) {
    _floatIcon = fapIcon;
    _isBottomSheetOpen = value!;
    emit(AppFloatState());
  }

  Color getBottomNvColor(value) {
    if (_currentIndex == value) {
      return Colors.white;
    } else
      return Colors.green;
  }
// to create a table
  void createDatabase() {
    openDatabase("todo.db", version: 1, onCreate: (database, version) {
      print("database created");
      database
          .execute(
              "CREATE TABLE $table ($columnId INTEGER PRIMARY KEY, $columnGoal TEXT, $columnDate TEXT, $columnTime TEXT, $columnStatus TEXT)")
          .then((value) {
        print('table created');
      });
    }, onOpen: (database) {
      getDatabaseFromDatabase(database);
      print('database opened');
    }).then((value) {
      _db = value;
      emit(AppCreateDatabaseState());
    });
  }
// insert data into table
  void insertToDatabase(
      {@required String? goal,
      @required String? date,
      @required String? time,}) async {
     _db!
        .rawInsert(
            "INSERT INTO $table ($columnGoal, $columnDate, $columnTime, $columnStatus) VALUES('$goal', '$date', '$time', 'new')")
        .then((value) {
      print('database inserted');
      emit(AppInsertToDatabaseState());
      getDatabaseFromDatabase(_db);
    });
  }
// to move a task from screen to another screen
  void updateDatabase({
    @required String? status,
    @required int? id,
  }) async {
    _db!.rawUpdate("UPDATE $table SET $columnStatus = ? WHERE $columnId = ?",
        ["$status", id]).then((value) {
      emit(AppUpdateDataAtDatabaseState());
      getDatabaseFromDatabase(_db);
    });
  }
// to update task's data
  update(
      {@required String? time,
      @required String? goal,
      @required String? date,
      @required int? id}) async {
    Database db = _db!;
    Map<String, dynamic> row = {
      columnTime: time,
      columnDate: date,
      columnGoal: goal,
    };
    int _id = id!;
     db.update(table, row,
        where: '$columnId = ?', whereArgs: [_id]).then((value) {
      emit(AppUpdateTasksDataAtDatabaseState());
      getDatabaseFromDatabase(db);
    });
  }
// for delete a task
  void deleteFromDatabase({
    @required int? id,
  }) async {
    _db!.rawDelete("DELETE FROM $table WHERE $columnId = ?", [id]).then((value) {
      emit(AppDeleteFromDataAtDatabaseState());
      getDatabaseFromDatabase(_db);
    });
  }
// to show data
  void getDatabaseFromDatabase(database) {
    doneDatabase = [];
    goalDatabase = [];
    archivedDatabase = [];

    database.rawQuery("SELECT * FROM $table").then((value) {
      value.forEach((element) {
        if (element['$columnStatus'] == 'new') {
          goalDatabase.add(element);
          _isGoalChecked = List<bool>.filled(goalDatabase.length, false);
          emit(AppGoalCheckBoxState());
          emit(AppGetFromDatabase());
        } else if (element['$columnStatus'] == 'done') {
          doneDatabase.add(element);
          _isDoneChecked = List<bool>.filled(doneDatabase.length, true);
          emit(AppDoneCheckBoxState());
          emit(AppGetFromDatabase());
        } else
          archivedDatabase.add(element);
        _isArchivedChecked = List<bool>.filled(archivedDatabase.length, true);
        emit(AppArchivedCheckBoxState());
        emit(AppGetFromDatabase());
      });
    });
  }

  int getCurrentIndex() {
    return _currentIndex;
  }

  bool getIsOpen() {
    return _isBottomSheetOpen;
  }

  Widget getScreens() {
    return _screens[_currentIndex];
  }

  String getTitle() {
    return _titles[_currentIndex];
  }

  IconData getIcon() {
    return _floatIcon;
  }

  bool getGoalIsChecked(index) {
    return _isGoalChecked[index];
  }

  bool getDoneIsChecked(index) {
    return _isDoneChecked[index];
  }

  bool getArchivedIsChecked(index) {
    return _isArchivedChecked[index];
  }
}

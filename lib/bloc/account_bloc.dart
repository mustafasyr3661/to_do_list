import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sqflite/sqflite.dart';

import 'account_bloc_states.dart';

class AppAccountBloc extends Cubit<AppAccountState> {
  AppAccountBloc() : super(AppInitialState());

  static AppAccountBloc get(context) => BlocProvider.of(context);

  //=======================Var=============================
  Widget? _widget;
  late Database _database;
  static final columnName = 'name';
  static final columnLastName = 'lastName';
  static final columnUserName = 'userName';
  static final columnEmail = 'email';
  static final columnPassword = 'password';
  static final columnId = 'id';
  static final table = 'auth';
  List<Map> _authData = [];
  int index = 0;
  int? authid;
  int? newAccountId;

  IconData _passwordIcon = Icons.remove_red_eye_sharp;

  bool _isPasswordShow = true;
  bool _isEmailExists = false;
  bool _isUserNameExists = false;
  bool _isEmailExistsForSignIn = false;
  bool _isPasswordCorrect = false;
  bool? signUp;

  List<FocusNode> _focusNodes = [
    FocusNode(),
    FocusNode(),
    FocusNode(),
  ];

  //====================Function=================================

  void suffixIconMood(value, lengthValue) {
    if (lengthValue.length == 0 || lengthValue == null) {
      _widget = Container(
        width: 0.0,
        height: 0.0,
      );
      emit(AppSuffixIconIsNull());
    } else {
      _widget = IconButton(
        onPressed: onPressed,
        icon: Icon(
          _passwordIcon,
          color: _focusNodes[value].hasFocus ? Colors.green : Colors.lightGreen,
        ),
      );
      emit(AppSuffixIconIsNotNull());
    }
  }

  void focusNode() {
    _focusNodes.forEach((element) {
      element.addListener(() {
        emit(AppFocusNodesState());
      });
    });
  }

  void onPressed() {
    _isPasswordShow = !_isPasswordShow;

    if (_isPasswordShow) {
      _passwordIcon = Icons.remove_red_eye_sharp;
      //emit(AppPasswordIconMoodTrue());
    } else {
      _passwordIcon = Icons.remove_red_eye_outlined;
      //emit(AppPasswordIconMoodFalse());
    }
    emit(AppIsPasswordShowState());
  }

  bool findEmail(String value) {
    newAccountId = _authData.length + 1;
    index = _authData.length;
    signUp = true;
    print("index+$index");
    if (_authData.isEmpty) {
      _isEmailExists = false;
    } else {
      for (int i = 0; i < _authData.length; i++) {
        if (_authData[i][columnEmail] == value) {
          _isEmailExists = true;
          break;
        } else {
          _isEmailExists = false;
        }
      }
    }
    return _isEmailExists;
  }

  bool findEmailForSettings(String value) {
    if (_authData.isEmpty) {
      _isEmailExists = false;
    } else {
      for (int i = 0; i < _authData.length; i++) {
        if (_authData[i][columnEmail] == value) {
          _isEmailExists = true;
          break;
        } else {
          _isEmailExists = false;
        }
      }
    }
    return _isEmailExists;
  }

  bool findUserName(String value) {
    if (_authData.isEmpty) {
      _isUserNameExists = false;
    } else {
      for (int i = 0; i < _authData.length; i++) {
        if (_authData[i][columnUserName] == value) {
          _isUserNameExists = true;
          break;
        } else {
          _isUserNameExists = false;
        }
      }
    }
    return _isUserNameExists;
  }

  bool findEmailAndUserNameForSignIn(String value) {
    signUp = false;
    if (_authData.isEmpty) {
      index = 0;
      authid = 1;
      _isUserNameExists = false;
    } else {
      for (int i = 0; i < _authData.length; i++) {
        if (_authData[i][columnEmail] == value ||
            _authData[i][columnUserName] == value) {
          index = i;
          authid = _authData[i][columnId];
          print(_authData[i][columnId]);
          _isEmailExistsForSignIn = true;

          break;
        } else {
          _isEmailExistsForSignIn = false;
        }
      }
    }
    return _isEmailExistsForSignIn;
  }

  bool findPasswordForSignIn(String value, String password) {
    for (int i = 0; i < _authData.length; i++) {
      int passwordIndex = 0;
      if (_authData[i][columnEmail] == value ||
          _authData[i][columnUserName] == value) {
        passwordIndex = i;
        if (_authData[passwordIndex][columnPassword] == password) {
          _isPasswordCorrect = true;
          break;
        } else {
          _isPasswordCorrect = false;
        }
      }
    }
    return _isPasswordCorrect;
  }

  int? getAccountId() {
    if(signUp==true)
      return newAccountId;
    else
      return authid;
  }

  //===========================Get============================

  Widget? getSuffixWidget() {
    return _widget;
  }

  Color getPrefixIconColor(value) {
    return _focusNodes[value].hasFocus ? Colors.green : Colors.lightGreen;
  }

  bool getPasswordMood() {
    return _isPasswordShow;
  }

  FocusNode getFocusNode(value) {
    return _focusNodes[value];
  }

  String getAuthData(int i, String type) {
    return _authData[i][type];
  }

  List<Map> authData() {
    return _authData;
  }

  //=======================DataBase================
  void createDatabaseForAuth() {
    openDatabase("auth.db", version: 2, onCreate: (database, version) {
      print("database created");
      database
          .execute(
              "CREATE TABLE $table ($columnId INTEGER PRIMARY KEY, $columnName TEXT, $columnLastName TEXT, $columnUserName TEXT, $columnEmail TEXT, $columnPassword TEXT)")
          .then((value) {
        print("$table created");
      });
    }, onOpen: (database) {
      getFromAuthDatabase(database);
      print("database opened");
    }).then((value) {
      _database = value;
      print("database saved");
      focusNode();
      emit(AppCreateDatabaseState());
    });
  }

  void insertInToAuthDatabase(
      {@required String? email,
      @required String? userName,
      @required String? password,
      String name = "Your Name",
      String lastName = "lastName"}) {
    _database
        .rawInsert(
            "INSERT INTO $table ($columnName, $columnLastName, $columnEmail, $columnUserName,$columnPassword) VALUES('$name', '$lastName', '$email', '$userName', '$password')")
        .then((value) {
      print("database inserted");
      emit(AppInsertInToDatabaseState());
      getFromAuthDatabase(_database);
    });
  }

  update(
      {@required String? email,
      @required String? userName,
      @required int? id}) async {
    Database db = _database;
    Map<String, dynamic> row = {
      columnEmail: email,
      columnUserName: userName,
    };
    int _id = id!;
    db.update(table, row, where: '$columnId = ?', whereArgs: [_id]).then(
        (value) {
      emit(AppUpdateAuthDatabaseState());
      getFromAuthDatabase(db);
    });
  }

  updatePersonalityInf(
      {@required String? name,
        @required String? lastName,
        @required int? id}) async {
    Database db = _database;
    Map<String, dynamic> row = {
      columnName: name,
      columnLastName: lastName,
    };
    int _id = id!;
    db.update(table, row, where: '$columnId = ?', whereArgs: [_id]).then(
            (value) {
          emit(AppUpdatePersonalityIfState());
          getFromAuthDatabase(db);
        });
  }

  void getFromAuthDatabase(database) {
    database.rawQuery("SELECT * FROM $table").then((value) {
      _authData = value;
      print("database saved on $_authData");
      emit(AppGetFromAuthDatabaseState());
    });
  }
}

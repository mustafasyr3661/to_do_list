import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:to_do_app/bloc/account_bloc.dart';
import 'package:to_do_app/bloc/account_bloc_states.dart';
import 'package:to_do_app/bloc/bloc.dart';
import 'package:to_do_app/components/components.dart';

class SignUpScreen extends StatelessWidget {
  SignUpScreen({Key? key}) : super(key: key);
  var emailController = TextEditingController();
  var userNameController = TextEditingController();
  var passwordController = TextEditingController();
  var formStateKey = GlobalKey<FormState>();
  String? email;
  BuildContext? blocContext;


  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppAccountBloc, AppAccountState>(
      listener: (context, state) {},
      builder: (context, state) {
        blocContext = context;
        AppAccountBloc bloc = AppAccountBloc.get(context);
        return Scaffold(
          appBar: AppBar(),
          body: Container(
            margin: EdgeInsets.all(8),
            child: Center(
              child: SingleChildScrollView(
                child: Form(
                  key: formStateKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      buildFormField(
                        prefixIcon: Icons.email,
                        validator: (value) {
                          if (value!.isEmpty || value.length == 0) {
                            return "Enter email";
                          } else if (bloc.findEmail(emailController.text)) {
                            return "Your email is already used";
                          } else
                            return null;
                        },
                        controller: emailController,
                        keyboardType: TextInputType.emailAddress,
                        title: "Email",
                        focusNode: bloc.getFocusNode(0),
                        prefixIconColor: bloc.getPrefixIconColor(0),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      buildFormField(
                        prefixIcon: Icons.person,
                        validator: (value) {
                          if (value!.isEmpty || value.length == 0) {
                            return "Enter username";
                          } else if (bloc
                              .findUserName(userNameController.text)) {
                            return "Your userName is already used";
                          } else
                            return null;
                        },
                        controller: userNameController,
                        keyboardType: TextInputType.emailAddress,
                        title: "UserName",
                        focusNode: bloc.getFocusNode(1),
                        prefixIconColor: bloc.getPrefixIconColor(1),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      buildFormField(
                          prefixIcon: Icons.password,
                          validator: (value) {
                            if (value!.isEmpty || value.length == 0) {
                              return "Enter password";
                            } else if (passwordController.text.length < 8) {
                              return "Your password have to be more then 8 liters";
                            } else
                              return null;
                          },
                          keyboardType: TextInputType.visiblePassword,
                          controller: passwordController,
                          isPasswordShow: bloc.getPasswordMood(),
                          onChanged: (value) {
                            bloc.suffixIconMood(2, value);
                            print(value);
                          },
                          suffixIcon: bloc.getSuffixWidget(),
                          title: "Password",
                          focusNode: bloc.getFocusNode(2),
                          prefixIconColor: bloc.getPrefixIconColor(2)),
                      SizedBox(
                        height: 75,
                      ),
                      SizedBox(
                          width: 200,
                          height: 50,
                          child: buildButton(
                            onPressed: () {
                              if (formStateKey.currentState!.validate()) {
                                email = emailController.text;
                                bloc.insertInToAuthDatabase(
                                    email: emailController.text,
                                    userName: userNameController.text,
                                    password: passwordController.text);
                                AppBloc.get(context).createDatabase();
                                Navigator.of(context)
                                    .pushReplacementNamed("HomePage");
                              }
                            },
                            buttonTitle: "Sign Up",
                          )),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "If you have an account ",
                            style: TextStyle(color: Colors.green),
                          ),
                          TextButton(
                              onPressed: () {
                                Navigator.of(context)
                                    .pushReplacementNamed("SignInScreen");
                              },
                              child: Text(
                                "Log in now",
                              ))
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

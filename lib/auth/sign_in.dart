import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:to_do_app/bloc/account_bloc.dart';
import 'package:to_do_app/bloc/account_bloc_states.dart';
import 'package:to_do_app/bloc/bloc.dart';
import 'package:to_do_app/components/components.dart';

class SignInScreen extends StatelessWidget {
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var formStateKey = GlobalKey<FormState>();
  String email="";

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppAccountBloc, AppAccountState>(
      listener: (context, state) {},
      builder: (context, state) {
        AppAccountBloc bloc = AppAccountBloc.get(context);
        return Scaffold(
          appBar: AppBar(
            title: Text("SignIn"),
          ),
          body: Container(
            margin: EdgeInsets.all(8),
            child: Center(
              child: SingleChildScrollView(
                child: Form(
                  key: formStateKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      buildFormField(
                        prefixIcon: Icons.email,
                        validator: (value) {
                          if (value!.isEmpty || value.length == 0) {
                            return "Enter your email";
                          } else if (bloc.findEmailAndUserNameForSignIn(
                                  emailController.text) ==
                              false) {
                            return "Your email is not exists";
                          } else
                            return null;
                        },
                        controller: emailController,
                        keyboardType: TextInputType.emailAddress,
                        title: "Email or userName",
                        focusNode: bloc.getFocusNode(0),
                        prefixIconColor: bloc.getPrefixIconColor(0),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      buildFormField(
                          prefixIcon: Icons.password,
                          validator: (value) {
                            if (value!.isEmpty || value.length == 0) {
                              return "Enter your password";
                            } else if (bloc.findPasswordForSignIn(
                                    emailController.text,
                                    passwordController.text) ==
                                false) {
                              return "Your password is not correct";
                            } else
                              return null;
                          },
                          keyboardType: TextInputType.visiblePassword,
                          controller: passwordController,
                          isPasswordShow: bloc.getPasswordMood(),
                          onChanged: (value) {
                            bloc.suffixIconMood(1, value);
                            print(value);
                          },
                          suffixIcon: bloc.getSuffixWidget(),
                          title: "Password",
                          focusNode: bloc.getFocusNode(1),
                          prefixIconColor: bloc.getPrefixIconColor(1)),
                      SizedBox(
                        height: 75,
                      ),
                      SizedBox(
                          width: 200,
                          height: 50,
                          child: buildButton(
                            onPressed: () {
                              if (formStateKey.currentState!.validate()) {
                                formStateKey.currentState!.save();
                                email=emailController.text;
                                AppBloc.get(context).createDatabase();
                                Navigator.of(context)
                                    .pushReplacementNamed("HomePage");
                              }
                            },
                            buttonTitle: "Sign In",
                          )),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "-OR-",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.green),
                      ),
                      Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Sign up",
                              style: TextStyle(color: Colors.green),
                            ),
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context)
                                      .pushReplacementNamed("SignUpScreen");
                                },
                                child: Text(
                                  "Now",
                                ))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
